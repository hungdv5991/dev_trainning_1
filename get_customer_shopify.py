import requests
import csv
shop_url = "https://fadfacb1b89c6e005553d396c91d3468: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@hungltu14.myshopify.com/admin/api/2020-07/customers.json"

r = requests.get(shop_url)

customer_data = r.json()['customers']

f = open("shopify_customer.csv", "w")
csv_writer = csv.writer(f)
# Counter variable used for writing
# headers to the CSV file
count = 0

for cus in customer_data:
    cus.pop('addresses', None)
    cus.pop('default_address', None)
    if count == 0:
        # Writing headers of CSV file
        header = cus.keys()
        csv_writer.writerow(header)
        count += 1
    # Writing data of CSV file
    csv_writer.writerow(cus.values())
f.close()
