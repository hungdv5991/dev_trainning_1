import mysql.connector
import csv

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="123",
    database="mydb"
)

mycursor = mydb.cursor()
mycursor.execute('DROP TABLE IF EXISTS customer')

with open('customer.csv') as csv_file:
    csv_reader = list(csv.reader(csv_file))
    mycursor.execute('''
    CREATE TABLE customer(
        customerid TEXT,
        firstname TEXT,
        lastname TEXT,
        companyname TEXT,
        billingaddress1 TEXT,
        billingaddress2 TEXT,
        city TEXT,
        state TEXT,
        postalcode TEXT,
        country TEXT,
        phonenumber TEXT,
        emailaddress TEXT,
        createddate TEXT
    )'''
    # .format(csv_reader[0][0], csv_reader[0][1], csv_reader[0][2], csv_reader[0][3], csv_reader[0][4], csv_reader[0][5], csv_reader[0][6], csv_reader[0][7], csv_reader[0][8], csv_reader[0][9], csv_reader[0][10], csv_reader[0][11], csv_reader[0][12])
    )

    for row in csv_reader[1:]:
        # print(row)
        mycursor.execute('''INSERT INTO customer VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)''',row) 
    mydb.commit()
